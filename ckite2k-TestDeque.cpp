// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           deque<int>,
        my_deque<int>,
           deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test_1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test_2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, test_3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, pushbackEmpty_4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(99);
    ASSERT_EQ(x.empty(), false);
}


TYPED_TEST(DequeFixture, pushback_5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(99);
    ASSERT_EQ(x.size(), 1u);
}

TYPED_TEST(DequeFixture, pushback2_6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(99);
    x.push_back(100);
    x.push_back(101);
    x.push_back(102);
    x.push_back(103);
    x.push_back(1000000);
    ASSERT_EQ(x.size(), 6u);
}

TYPED_TEST(DequeFixture, pushfrontEmpty_7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(99);
    ASSERT_EQ(x.empty(), false);
}

TYPED_TEST(DequeFixture, pushfront_8) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(99);
    ASSERT_EQ(x.size(), 1u);
}

TYPED_TEST(DequeFixture, pushfront2_9) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(99);
    ASSERT_EQ(x[0], 99);
}

TYPED_TEST(DequeFixture, size_10) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_EQ(x.size(), 10u);
}

TYPED_TEST(DequeFixture, size2_11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 99);
    ASSERT_EQ(x.size(), 10u);
}

TYPED_TEST(DequeFixture, constructor1_12) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{99, 100, 101};
    ASSERT_EQ(x.size(), 3u);
}

TYPED_TEST(DequeFixture, constructor2_13) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 2);
    deque_type x2(x);
    ASSERT_EQ(x2[0], 2);
    ASSERT_EQ(x2[1], 2);
    ASSERT_EQ(x2[2], 2);
}

TYPED_TEST(DequeFixture, assignment_14) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 2);
    deque_type x2 = x;
    ASSERT_EQ(x2[0], 2);
    ASSERT_EQ(x2[1], 2);
    ASSERT_EQ(x2[2], 2);
}

TYPED_TEST(DequeFixture, at_15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(99);
    ASSERT_EQ(x.at(0), 99);
}

TYPED_TEST(DequeFixture, at2_15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(99);
    x.push_back(100);
    ASSERT_EQ(x.at(0), 99);
    ASSERT_EQ(x.at(1), 100);
}

TYPED_TEST(DequeFixture, back_16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 2);
    ASSERT_EQ(x.back(), 2);
}

TYPED_TEST(DequeFixture, back2_17) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{99, 100, 101};
    ASSERT_EQ(x.back(), 101);
}

TYPED_TEST(DequeFixture, front_18) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(5, 2);
    ASSERT_EQ(x.front(), 2);
}

TYPED_TEST(DequeFixture, front2_19) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{99, 100, 101};
    ASSERT_EQ(x.front(), 99);
}

TYPED_TEST(DequeFixture, iterator_20) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5, 2);
    iterator b = begin(x);
    ASSERT_EQ(*b, 2);
}

TYPED_TEST(DequeFixture, iterator2_21) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5, 2);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(std::distance(b, e), 5);
}

TYPED_TEST(DequeFixture, iterator3_22) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5, 2);
    iterator b = begin(x);
    iterator e = end(x);
    b += 1;
    ASSERT_EQ(std::distance(b, e), 4);
}

TYPED_TEST(DequeFixture, iterator4_23) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(5, 2);
    const_iterator b = begin(x);
    ASSERT_EQ(*b, 2);
}

TYPED_TEST(DequeFixture, iterator5_24) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(5, 2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(std::distance(b, e), 5);
}

TYPED_TEST(DequeFixture, iterator6_25) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(5, 2);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b += 1;
    ASSERT_EQ(std::distance(b, e), 4);
}

TYPED_TEST(DequeFixture, clear_26) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(5, 2);
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, clear2_27) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, empty_28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.empty(), true);
}


TYPED_TEST(DequeFixture, equal_29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x, x2;
    ASSERT_EQ(x, x2);
}

TYPED_TEST(DequeFixture, equal2_30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(2, 2);
    deque_type y;
    y.push_back(2);
    y.push_back(2);
    ASSERT_EQ(x, y);
}
