// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test
{
    using deque_type = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types, );
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, constructor_decoupling_1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(0);
    const deque_type y(0);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, constructor_decoupling_2) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(0);
    const deque_type y;
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, constructor_decoupling_3) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    const deque_type y(0);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, constructor_decoupling_4) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    const deque_type y;
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    const deque_type y;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, iter_equal) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, iter_equal_const) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::const_iterator;
    const deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, erase) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {2, 4, 6, 8};
    iterator b = begin(x);
    ++b;

    x.erase(b);
    ASSERT_EQ(x[1], 6);
    ASSERT_EQ(x.size(), 3);
}

TYPED_TEST(DequeFixture, push_front) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {2, 4, 6, 8};
    ASSERT_EQ(x.front(), 2);
    x.push_front(-1);
    x.push_front(-2);
    x.push_front(-3);
    ASSERT_EQ(x[0], -3);
    ASSERT_EQ(x[1], -2);
    ASSERT_EQ(x[2], -1);
    ASSERT_EQ(x[3], 2);
}

TYPED_TEST(DequeFixture, insert_front) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {2, 4, 6, 8};
    ASSERT_EQ(x.front(), 2);
    iterator begin = x.insert(x.begin(), -1);
    ASSERT_EQ(x.front(), -1);
    ASSERT_TRUE(x.begin() == begin);
}

TYPED_TEST(DequeFixture, fronts) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};
    ASSERT_EQ(x.front(), 2);
    x.pop_front();
    ASSERT_EQ(x.front(), 4);
    x.push_front(-1);
    ASSERT_EQ(x.front(), -1);
}


TYPED_TEST(DequeFixture, insert_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};
    x.insert(x.end(), 10);
    ASSERT_EQ(x.back(), 10);
}

TYPED_TEST(DequeFixture, push_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};
    x.push_back(10);
    x.push_back(100);
    x.push_back(1000);
    ASSERT_EQ(x[3], 8);
    ASSERT_EQ(x[4], 10);
    ASSERT_EQ(x[5], 100);
    ASSERT_EQ(x[6], 1000);
}

TYPED_TEST(DequeFixture, backs) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};
    ASSERT_EQ(x.back(), 8);
    x.pop_back();
    ASSERT_EQ(x.back(), 6);
    x.push_back(10);
    ASSERT_EQ(x.back(), 10);
}

TYPED_TEST(DequeFixture, size_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3);
    ASSERT_EQ(x.size(), 3);
    x.resize(24);
    ASSERT_EQ(x.size(), 24);
}

TYPED_TEST(DequeFixture, size_2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};
    ASSERT_EQ(x.size(), 4);
    x.resize(10);
    ASSERT_EQ(x.size(), 10);
}

TYPED_TEST(DequeFixture, insert) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {2, 4, 6, 8};
    iterator b = begin(x);
    b += 3;
    x.insert(b, 15);
    ASSERT_EQ(x[3], 15);
}

TYPED_TEST(DequeFixture, equals_1)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    const deque_type y;
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, equals_2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(1);
    y.push_back(1);
    ASSERT_EQ(x, y);
}

// <
TYPED_TEST(DequeFixture, less_than_1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(1);
    x.push_back(2);
    y.push_back(1);
    y.push_back(3);
    ASSERT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, less_than_2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(1);
    x.push_back(2);
    y.push_back(1);
    ASSERT_TRUE(y < x);
}

// =
TYPED_TEST(DequeFixture, assign)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(1);
    x.push_back(2);
    y = x;
    ASSERT_EQ(x, y);
}

// at
TYPED_TEST(DequeFixture, at_1)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    ASSERT_EQ(x.at(1), 2);
}

TYPED_TEST(DequeFixture, at_2)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    try {
        x.at(2);
        ASSERT_TRUE(false);
    } catch (const out_of_range & ex) {
        ASSERT_TRUE(true);
    }
}

TYPED_TEST(DequeFixture, clear) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {2, 4, 6, 8};

    x.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, size_constructor) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(20);
    iterator b = begin(x);
    iterator e = end(x);
    e -= 20;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, const_iter_equal) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type const_x(0);
    const_iterator const_b = begin(const_x);
    const_iterator const_e = end(const_x);
    ASSERT_TRUE(const_b == const_e);
}

TYPED_TEST(DequeFixture, iter_plus) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4};
    iterator b = begin(x);
    b += 4;
    iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, iter_minus) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4};
    iterator e = end(x);
    e -= 2;
    ASSERT_EQ(*e, 3);
}

TYPED_TEST(DequeFixture, const_iter_plus) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type const_x = {1, 2, 3, 4};
    const_iterator const_b = begin(const_x);
    const_b += 4;
    const_iterator const_e = end(const_x);
    ASSERT_TRUE(const_b == const_e);
}

TYPED_TEST(DequeFixture, const_iter_minus) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type const_x = {1, 2, 3, 4};
    const_iterator const_e = end(const_x);
    const_e -= 2;
    ASSERT_EQ(*const_e, 3);
}

TYPED_TEST(DequeFixture, iter_inc_dec) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x = {1, 2, 3, 4};
    iterator b = begin(x);
    ++++++b;
    b++;
    iterator e = end(x);
    ASSERT_TRUE(b == e);
    b--;
    --b;
    ASSERT_EQ(*b, 3);
    const deque_type const_x = {1, 2, 3, 4};
    const_iterator const_b = begin(const_x);
    ++++++const_b;
    const_b++;
    const_iterator const_e = end(const_x);
    ASSERT_TRUE(const_b == const_e);
    const_b--;
    --const_b;
    ASSERT_EQ(*const_b, 3);
}
